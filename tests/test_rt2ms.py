#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `rt2ms` package."""

import unittest
import sys

try:
    import rt2ms
except ImportError:
     pass

class TestRt2ms(unittest.TestCase):
    """Tests for `rt2ms` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_import(self):
        if 'rt2ms' in sys.modules:
            self.assert_(True, "rt2ms loaded")
        else:
            self.fail()

