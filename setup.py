#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()


setup(
    author="IRIS PASSCAL",
    author_email='software-support@passcal.nmt.edu',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved ::  GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.7',
    ],
    description="Convert RT-130 raw data to mseed",
    entry_points={
        'console_scripts': [
            'rt2ms=rt2ms.rt2ms:main',
        ],
    },
    install_requires=['construct==2.5.2', 'numpy'],
    setup_requires = ['numpy'],
    extras_require={
        'dev': [
            'pip',
            'bumpversion',
            'wheel',
            'watchdog',
            'flake8',
            'tox',
            'coverage',
            'Sphinx',
            'twine',
        ]
    },
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='rt2ms',
    name='rt2ms',
    packages=find_packages(include=['rt2ms']),
    test_suite='tests',
    url='https://git.passcal.nmt.edu/passoft/rt2ms',
    version='2018.180',
    zip_safe=False,
)

# install C dependencies

from rt2ms import surt_130_py
from rt2ms import sumseed_py
surt_130_py.install()
sumseed_py.install()
