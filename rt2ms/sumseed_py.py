from distutils.core import setup, Extension
import os
import numpy


def install():
    dir_path = os.path.dirname(os.path.realpath(__file__))

    setup(name="mseed_py", version="2014.330", include_dirs=[numpy.get_include()],
          ext_modules=[
              Extension(
                  "mseed_py", ["{0}/mseedwrapper_py.c".format(dir_path)],
                   library_dirs=['../../libmseed'],
                  libraries=['mseed']
              )])


if __name__ == '__main__':
    install()
