#!/usr/bin/env picpython

import shutil, os, stat, sys
from compileall import *

PROG_VERSION = '2009.310'

#   Location of PASSCAL distribution
ROOTDIR = '/opt/passcal'
#
'''
if len(sys.argv) > 1 :
    ROOTDIR = sys.argv[1]
elif os.environ.has_key ('PASSCAL') :
    ROOTDIR = os.environ['PASSCAL']
else :
    sys.stderr.write ("PASSCAL environment variable not set!\n")
    sys.exit ()
'''
PROGDIR = os.getcwd ()
PROG = os.path.basename (PROGDIR)
LIBDIR = os.path.join (ROOTDIR, 'lib', 'python')
BINDIR = os.path.join (ROOTDIR, 'bin')
OTHERBINDIR = os.path.join (ROOTDIR, 'other', 'bin')
LIBPROG = os.path.join (LIBDIR, PROG)
PYTHON = os.path.join (OTHERBINDIR, 'picpython')
#PYTHON = os.path.join (BINDIR, 'pnpython2')

PROGS = ('rt2ms',)

LIBS  = ('pn130',
         'RT_130_h')

EXTS = ('surt_130_py.py', 'sumseed_py.py')

os.system ("./clean.sh")
#   Delete libs
for p in LIBS :
    p = p + '.pyc'
    try :
        os.remove (p)
    except OSError :
        pass
    
#   Compile
compile_dir (".")
#   Make libs dir
command = 'mkdir -p ' + LIBDIR
os.system (command)
#   Remove old libs
try :
    shutil.rmtree (LIBPROG)
except OSError :
    pass
'''
command = 'mkdir -p ' + BINDIR
os.system (command)

#   install programs
for p in PROGS :
    src = p
    dst = BINDIR + '/' + p
    try :
        os.remove (dst)
    except OSError :
        pass
    
    print src, dst
    shutil.copy (src, dst)
    os.chmod (dst, 0755)
'''
#   compile extensions
for e in EXTS :
    print "Compiling extension " + e
    command = PYTHON + " " + e + " clean --build-lib=."
    print command
    os.system (command)
    command = PYTHON + " " + e + " build --build-lib=."
    print command
    os.system (command)
'''
#   install libraries
shutil.copytree (PROGDIR, LIBPROG)
'''
