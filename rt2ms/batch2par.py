#!/usr/bin/env picpython

#
#   Convert Antelope batch file to par file format
#   Steve Azevedo, May 2009
#

import sys
import time as t

PROG_VERSION = '2014.006 Test'

#   List of batch keyed on DAS
BATCH = {}
WORKING_BATCH = None
CURRENT_DAS = None
#CURRENT_TIME = None
CODE = None
NET = None
MASSP = None

BATCHSUM = False
STASORT = False

#   Only for 3 channel RefTeks
CHAN_MAP = { 'Z':'1', 'N':'2', 'E':'3' }

class sta (object) :
    __slots__ = 'code', 'lat', 'lon', 'elev', 'staname'
    
class time (object) :
    __slots__ = 'year', 'month', 'day', 'hour', 'minute', 'seconds', 'epoch'
    
class net (object) :
    __slots__ = 'netcode', 'netname'
    
class datalogger (object) :
    __slots__ = 'code', 'serialnumber', 'dlsta', 'dlloc'
    
class sensor (object) :
    __slots__ = 'code', 'edepth', 'serialnumber', 'loc', 'stream'
    
class samplerate (object) :
    __slots__ = 'code', 'loc'
    
class channel (object) :
    __slots__ = 'label', 'chan', 'loc', 'dlchan'
    
class channels (object) :
    def __init__ (self) :
        self.channel = {}
        self.samplerate = samplerate ()
    
class batch (object) :
    def __init__ (self) :
        self.sta = sta ()
        self.time = time ()
        #self.net = net ()
        self.datalogger = datalogger ()
        #self.sensor = sensor ()
        self.sensor = []
        #   Keyed on samplerate code
        #      Points to an instance of channels
        #self.stream = {}
        
def parse_sta (flds) :
    global WORKING_BATCH
    l = len (flds)
    if not WORKING_BATCH :
        wb = WORKING_BATCH = batch ()
    else :
        wb = WORKING_BATCH
    
    wb.sta.code = flds[1]
    if l > 3 :
        wb.sta.lat = flds[2]
        wb.sta.lon = flds[3]
        
    if l > 4 :
        wb.sta.elev = flds[4]
        
    if l > 5 :
        wb.sta.staname = flds[5]
        
def parse_time (flds) :
    global WORKING_BATCH
    #tdoy = TimeDoy.TimeDoy ()
    l = len (flds)
    if not WORKING_BATCH :
        wb = WORKING_BATCH = batch ()
    else :
        wb = WORKING_BATCH
        
    time_date = flds[1]
    time_time = flds[2]
    
    wb.time.month, wb.time.day, wb.time.year = time_date.split ('/')
    try :
        wb.time.hour, wb.time.minute, wb.time.seconds = time_time.split (':')
    except ValueError :
        wb.time.hour, wb.time.minute = time_time.split (':')
        wb.time.seconds = '0.0'
        
    wb.time.epoch = t.mktime ((int (wb.time.year),
                               int (wb.time.month),
                               int (wb.time.day),
                               int (wb.time.hour),
                               int (wb.time.minute),
                               int (float (wb.time.seconds)),
                               -1,
                               -1,
                               -1))
    
def parse_net (flds) :
    global NET
    l = len (flds)
    if not NET :
        NET = net ()
        
    NET.netcode = flds[1]
    NET.netname = flds[2]
    
def parse_datalogger (flds) :
    global WORKING_BATCH, CURRENT_DAS
    l = len (flds)
    if not WORKING_BATCH :
        wb = WORKING_BATCH = batch ()
    else :
        wb = WORKING_BATCH
        
    wb.datalogger.code = flds[1]
    wb.datalogger.serialnumber = flds[2]
    CURRENT_DAS = flds[2]
    if l > 3 :
        wb.datalogger.dlsta = flds[3]
        
    if l > 4 :
        wb.datalogger.dlloc = flds[4]
        
def parse_sensor (flds) :
    global WORKING_BATCH
    l = len (flds)
    if not WORKING_BATCH :
        wb = WORKING_BATCH = batch ()
    else :
        wb = WORKING_BATCH
        
    lsensor = sensor ()
    lsensor.stream = {}
    lsensor.code = flds[1]
    lsensor.edepth = flds[2]
    lsensor.serialnumber = flds[3]
    if l > 4 :
        lsensor.loc = flds[4]
        
    wb.sensor.append (lsensor)
        
def parse_samplerate (flds) :
    global WORKING_BATCH, CODE
    l = len (flds)
    if not WORKING_BATCH :
        wb = WORKING_BATCH = batch ()
    else :
        wb = WORKING_BATCH
        
    CODE = flds[1]
    #try :
        #sr = float (CODE[:-3])
    #except Exception as e :
        #sr = 1
        
    #if sr == .1 : MASSP = False
    #   We should already have a sensor
    sens = wb.sensor[-1]
    lstream = sens.stream
    
    lstream[CODE] = channels ()
    
    lstream[CODE].samplerate.code = CODE
    
    if l > 2 :
        lstream[CODE].samplerate.loc = flds[2]
        
def parse_channel (flds) :
    global WORKING_BATCH, CODE
    l = len (flds)
    if not WORKING_BATCH :
        wb = WORKING_BATCH = batch ()
    else :
        wb = WORKING_BATCH
        
    if flds[2] == 'off' :
        return
    
    sens = wb.sensor[-1]
    if sens.stream :
        lstream = sens.stream[CODE]
    else :
        lstream = sens.stream
        
    label = flds[1]
    lstream.channel[label] = channel ()
    lstream.channel[label].label = label
    if l > 2 :
        lstream.channel[label].chan = flds[2]
        
    if l > 3 :
        lstream.channel[label].loc = flds[3]
        
    if l > 4 :
        lstream.channel[label].dlchan = flds[4]
        
def close_entry () :
    global WORKING_BATCH, CURRENT_DAS
        
    if BATCH.has_key (CURRENT_DAS) :
        BATCH[CURRENT_DAS].append (WORKING_BATCH)
    else :
        BATCH[CURRENT_DAS] = []
        BATCH[CURRENT_DAS].append (WORKING_BATCH)
    
    WORKING_BATCH = None
    CURRENT_DAS = None
    CODE = None
    
def belch_par () :
    dass = BATCH.keys ()
    
    dass.sort ()
    if BATCHSUM :
        print "#das; refchan; netcode; station; channel; samplerate; time"
    else :
        print "#das; refchan; refstrm; netcode; station; channel; samplerate; gain"
        
    sumstring = ''
    for d in dass :
        if d == None :
            pass
        nine = False
        wbs = BATCH[d]
        print d, wbs
        for wb in wbs :
            station = wb.sta.code
            netcode = NET.netcode
            time = wb.time.epoch
            for sensor in wb.sensor :
                #codes = sensor.stream.keys ()
                for sr in sensor.stream.keys () :
                    #sr = sensor.stream[c]
                    samplerate = sr[:-3]
                    if float (samplerate) == 0.1 :
                        nine = True
                    for ch in sensor.stream[sr].channel :
                        chan = sensor.stream[sr].channel[ch].chan
                        l = sensor.stream[sr].channel[ch].label
                        channel = CHAN_MAP[l]
                        #das;refchan;refstrm;netcode;station;channel;samplerate;gain
                        if BATCHSUM :
                            sumstring +=  "%s;\t%s;\t%s;\t%s;\t%s;\t%s;\t\t%s\n" % (d,
                                                                                    channel,
                                                                                    #sr,
                                                                                    netcode,
                                                                                    station,
                                                                                    chan,
                                                                                    samplerate,
                                                                                    t.strftime("%Y/%m/%d (%j) %H:%M", t.localtime(time)))
                        else :
                            print "%s;\t%s;\trs%srs;\t%s;\t%s;\t%s;\t%s;\t%s" % (d,
                                                                                 channel,
                                                                                 sr,
                                                                                 netcode,
                                                                                 station,
                                                                                 chan,
                                                                                 samplerate,
                                                                                 'x1')
        if MASSP and not nine :
            channel = 0
            for chan in ('VM1', 'VM2', 'VM3') :
                channel += 1
                if BATCHSUM :
                    sumstring +=  "%s;\t%s;\t%s;\t%s;\t%s;\t%s;\t\t%s\n" % (d,
                                                                            channel,
                                                                            #sr,
                                                                            netcode,
                                                                            station,
                                                                            chan,
                                                                            '0.1',
                                                                            t.strftime("%Y/%m/%d (%j) %H:%M", t.localtime(time)))
                else :
                    print "%s;\t%s;\t%s;\t\t%s;\t%s;\t%s;\t%s;\t%s" % (d,
                                                                       channel,
                                                                       '9',
                                                                       netcode,
                                                                       station,
                                                                       chan,
                                                                       '0.1',
                                                                       'x1')                
    if BATCHSUM:
        #sort by station
        if STASORT:
            decorated = [ (line.split('\t')[3], line + '\n') for line in sumstring.split('\n') if len(line.split('\t')) > 3]
            decorated.sort()
            print ''.join( [ line for  net, line in decorated] )
        #sorted by das
        else:
            print sumstring
        
                            
if __name__ == '__main__' :
    #if len (sys.argv) > 2 :
    if '-a' in sys.argv:
        BATCHSUM = True
    if '-s' in sys.argv:
        BATCHSUM = True
        STASORT = True
    if '-m' in sys.argv :
        MASSP = True
        
    try :
        batch_file = sys.argv[1]
        fh = open (batch_file)
    except :
        sys.stderr.write ("Version: %s\nUsage: batch2par batch_file [ -s  | -a | -m ]  >  par_file.txt\n" % PROG_VERSION)
        sys.stderr.write ("The following options produce batchsum files:\n")
        sys.stderr.write ("-a   :   abstract/summary sorted by DAS SN\n")
        sys.stderr.write ("-s   :   abstract/summary sorted by station name\n")
        sys.stderr.write ("-m   :   Write line for stream 9 even if not described in batch file")
        sys.exit ()
        
    n = 0
    while 1 :
        line = fh.readline ()
        if not line : break
        n += 1
        line = line.strip ()
        flds = line.split ()
        if not flds : continue
        if flds[0] == 'sta' :
            try :
                parse_sta (flds)
            except Exception, e :
                sys.stderr.write ("Error: {0}\n{2}: {1}\n".format (e, line, n))
                
        elif flds[0] == 'time' :
            try :
                parse_time (flds)
            except Exception, e :
                sys.stderr.write ("Error: {0}\n{2}: {1}\n".format (e, line, n))
                
        elif flds[0] == 'net' :
            try :
                parse_net (flds)
            except Exception, e :
                sys.stderr.write ("Error: {0}\n{2}: {1}\n".format (e, line, n))
                
        elif flds[0] == 'datalogger' :
            try :
                parse_datalogger (flds)
            except Exception, e :
                sys.stderr.write ("Error: {0}\n{2}: {1}\n".format (e, line, n))
                
        elif flds[0] == 'sensor' :
            try :
                parse_sensor (flds)
            except Exception, e :
                sys.stderr.write ("Error: {0}\n{2}: {1}\n".format (e, line, n))
                
        elif flds[0] == 'samplerate' :
            try :
                parse_samplerate (flds)
            except Exception, e :
                sys.stderr.write ("Error: {0}\n{2}: {1}\n".format (e, line, n))
                
        elif flds[0] == 'channel' :
            try :
                parse_channel (flds)
            except Exception, e :
                sys.stderr.write ("Error: {0}\n{2}: {1}\n".format (e, line, n))
                
        elif flds[0] == 'add' :
            try :
                close_entry ()
            except Exception, e :
                sys.stderr.write ("Error: {0}\n{2}: {1}\n".format (e, line, n))
                
        elif flds[0] == 'close' or flds[0] == 'axis' :
            pass
        else :
            sys.stderr.write ("Skipping line: %s\n" % line)
    
    belch_par ()
    if BATCHSUM : exit ()
    sys.stderr.write ("\n***   WARNING WARINIG WARNING   ***\n\n")
    #sys.stderr.write ("You must do a global substitution for refchan 'rc?rc'\n")
    sys.stderr.write ("You must do a global substitution for refstrm 'rs?rs'\n")
    sys.stderr.write ("Gain set to 'x1'. If 'x32' was used do a global substitution.\n")