#!/usr/bin/env python

import os, os.path, re, string

nameRE = re.compile (".*\.m")

def fixname (junk, dir, names) :
    
    stream = dir[-2:]
    for n in names :
        if not nameRE.match (n) : continue
        flds = n.split ('.')
        flds[0] = '20' + flds[0]
        flds[7] = flds[6]
        flds.append ('m')
        flds[6] = stream
        newname = string.join (flds, '.')
        old = os.path.join (dir, n)
        new = os.path.join (dir, newname)
        command = "mv %s %s" % (old, new)
        print command
        os.system (command)

if __name__ == "__main__" :
    os.path.walk (".", fixname, 1)
