.. highlight:: shell

============
Installation
============

From sources
------------
You will first need to obtain, compile and install libmseed from:
https://github.com/iris-edu/libmseed

The sources for rt2ms can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://git.passcal.nmt.edu/passoft/rt2ms

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://git.passcal.nmt.edu/passoft/rt2ms/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://git.passcal.nmt.edu/passoft/rt2ms
.. _tarball: https://git.passcal.nmt.edu/passoft/rt2ms/tarball/master
